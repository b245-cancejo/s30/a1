db.fruits.aggregate([
			{$match: {onSale: true}},
			{$count: "fruitsOnSale"}	
		])

db.fruits.aggregate([
		{$match: {stock: {$gte:20}}},
		{$count: "enoughStock"}
	])


db.fruits.aggregate([
			{$match: {onSale: true}},
			{$group: {_id: "$stock",
			avgPrice: {$avg: "$price"}}
			}		
		])


db.fruits.aggregate([
		{$match: {onSale: true}},
		{$group: {_id: "$supplier_id", 
		maxPrice: {$max: "$price"}}},
		{$sort: {maxPrice: 1}}		
	])


db.fruits.aggregate([
		{$match: {onSale: true}},
		{$group: {_id: "$supplier_id", 
		minPrice: {$min: "$price"}}},
		{$sort: {maxPrice: 1}}		
	])